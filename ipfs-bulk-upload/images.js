let fs = require("fs");
let axios = require("axios");

let ipfsArray = [];
let promises = [];

for (let i = 1; i < 11; i++) {
    let paddedHex = ("0000000000000000000000000000000000000000000000000000000000000000" + i.toString());
    console.log(paddedHex);
    promises.push(new Promise( (res, rej) => {
        fs.readFile(`${__dirname}/images/${paddedHex}.png`, (err, data) => {
            if(err) rej();
            console.log(`${__dirname}/images/${paddedHex}.png`);
            console.log(data);
            ipfsArray.push({
                path: `images/${paddedHex}.png`,
                content: data.toString("base64")
            })
            res();
        })
    }))
}
Promise.all(promises).then( () => {
    axios.post("https://deep-index.moralis.io/api/v2/ipfs/uploadFolder", 
        ipfsArray,
        {
            headers: {
                "X-API-KEY": 'MqAoftYWAAjal3iH1WhvRSw64RjJpD68EO2LDcXZpPzUUZc4PJOsw5m0Qa3TQYI6',
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        }
    ).then( (res) => {
        console.log(res.data);
    })
    .catch ( (error) => {
        console.log(error)
    })
})
